#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU Lesser General Public License,
# version 2.1, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#

.DEFAULT_GOAL := all

HSTR_INSTALL:=/usr/share/doc/hStreams

# Location for ref_code directory
HOME_DIR:=$(TUT_INSTALL)/
SRC_DIR:=test_gemv/

COMMON_CXXFLAGS = -w2 -fpic -O2 -std=c++11 -qopenmp

COMMON_CXXFLAGS += -I/usr/include/hStreams

HOST_CXXFLAGS    := $(COMMON_CXXFLAGS)
x100_SINK_CXXFLAGS := $(COMMON_CXXFLAGS)

# We use icpc for compilation
HOST_CXX    := icpc
x100_SINK_CXX := icpc -mmic

# Tags which to use for namng the intermediate object files Helps avoid name
# clashes among compilations for different targets from one source file.
HOST_TAG    := source
x100_SINK_TAG := x100-sink

# dir creation guard, to prevent explicitly adding directories as targets
# put $(dir_create) as a first command in the target definition
dir_create=@mkdir -p $(@D)

RM_rf := rm -rf

BIN_HOST = $(HOME_DIR)/bin/host/
BIN_x100 = $(HOME_DIR)/bin/x100-card/
BUILD_HOST = $(HOME_DIR)/build/host/
BUILD_x100 = $(HOME_DIR)/build/x100-card/

TARGET_HSTR := $(BIN_HOST)hStreams
TARGET_LEO := $(BIN_HOST)LEO
TARGET_LEOSTR := $(BIN_HOST)LEO_streams

x100_SINK_TARGET_HSTR := $(BIN_x100)hStreams_mic.so

ADDITIONAL_HOST_CXXFLAGS := -xCORE-AVX2

#  -lhstreams_source is required for hStreams
ADDITIONAL_HOST_LDFLAGS  := -lhstreams_source -qopenmp -lmkl_intel_ilp64 \
	-lmkl_core -lmkl_sequential \
	-qoffload-option,mic,ld," -L${MIC_LD_LIBRARY_PATH} -rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ -rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core"

ADDITIONAL_LEO_LDFLAGS  := -qopenmp -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential \
	-qoffload-option,mic,ld," -L${MIC_LD_LIBRARY_PATH} -rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ -rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core"

# The soname is a string, which is used as a "logical name" describing the
# functionality of the object
ADDITIONAL_x100_SINK_HSTR_LDFLAGS:= -shared \
  -Wl,-soname,$(x100_SINK_TARGET_HSTR) -qopenmp -L${MIC_LD_LIBRARY_PATH} \
	-Wl,-rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ \
	-Wl,-rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 \
	-lmkl_intel_thread -lmkl_core

# Sources and objects
HSTR_SRCS := test_hStreams_src.cpp
HSTR_OBJS := $(addprefix $(BUILD_HOST), $(HSTR_SRCS:.cpp=.$(HOST_TAG).o))

LEO_SRCS := test_LEO.cpp
LEO_OBJS := $(addprefix $(BUILD_HOST),  $(LEO_SRCS:.cpp=.$(HOST_TAG).o))

LEOSTR_SRCS := test_offload_streams.cpp
LEOSTR_OBJS := $(addprefix $(BUILD_HOST), $(LEOSTR_SRCS:.cpp=.$(HOST_TAG).o))

# Sink-side source and object files
x100_SINK_SRCS = test_hStreams_sink.cpp
x100_SINK_OBJS = $(addprefix $(BUILD_x100), $(x100_SINK_SRCS:.cpp=.$(x100_SINK_TAG).o))

# Target for hStreams tests
hStreams: $(TARGET_HSTR) $(x100_SINK_TARGET_HSTR)

# Target for LEO tests
LEO: $(TARGET_LEO)

# Target for offload streams tests
LEO_streams: $(TARGET_LEOSTR)

# The default "all" target - builds everything
all: hStreams LEO LEO_streams

$(HSTR_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(HSTR_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(LEO_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(LEO_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(LEOSTR_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(LEOSTR_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(x100_SINK_OBJS): %.$(x100_SINK_TAG).o: $(SRC_DIR)$(x100_SINK_SRCS)
	$(dir_create)
	$(x100_SINK_CXX) -c $^ -o $@ $(x100_SINK_CXXFLAGS)

$(x100_SINK_TARGET_HSTR): $(x100_SINK_OBJS)
	$(dir_create)
	$(x100_SINK_CXX) $^ -o $@ $(x100_SINK_LDFLAGS) \
	$(ADDITIONAL_x100_SINK_HSTR_LDFLAGS)

$(TARGET_HSTR): $(HSTR_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_HOST_LDFLAGS)

$(TARGET_LEO): $(LEO_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_LEO_LDFLAGS)

$(TARGET_LEOSTR): $(LEOSTR_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_LEO_LDFLAGS)


.PHONY: clean
clean:
	$(RM_rf) $(TARGET_HSTR) $(HSTR_OBJS) \
	$(x100_SINK_HSTR_TARGET) $(x100_SINK_OBJS) \
	$(TARGET_LEO) $(LEO_OBJS) \
	$(TARGET_LEOSTR) $(LEOSTR_OBJS)
