/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

#include <hStreams_sink.h>
#include <stdio.h>
#include <iostream>
#include <sys/time.h>
#include <mkl.h>
#include <unistd.h>

void print_vector( int n, double * y );

HSTREAMS_EXPORT
void test_gemv_sink(
  uint64_t n_c,
  uint64_t type_c,
  uint64_t A_c, 
  uint64_t x_c,
  uint64_t y_c
  ){

  int type;
  long n;
  long one = 1;
  double * A, * x, * y;

  n = (long) n_c;
  type = (int) type_c;
  A = (double *) A_c;
  x = (double *) x_c;
  y = (double *) y_c;


  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;

  switch( type ){
    case 0:
      cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, A, n, x, one, 0.0, y, one );
      break;
    case 1:
      cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, A, n, x, one, 0.0, y, one );
      break; 
    case 2:
      cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, A, x, one, 0.0, y, one );
      break;
  }
}

HSTREAMS_EXPORT
void test_gemv_chunked_sink(
  uint64_t n_c, uint64_t type_c, uint64_t chunkSize_c,
  uint64_t A01_c, uint64_t x01_c, uint64_t y01_c, /*   4-6   */
  uint64_t A02_c, uint64_t x02_c, uint64_t y02_c, /*   7-9   */
  uint64_t A03_c, uint64_t x03_c, uint64_t y03_c /*  10-12  */
//  uint64_t A04_c, uint64_t x04_c, uint64_t y04_c, /*  13-15  */
//  uint64_t A05_c, uint64_t x05_c, uint64_t y05_c  /*  16-18  */
  ){

  int type, chunkSize;
  long n;
  double ** As;
  double ** Xs;
  double ** Ys;

  n = (long) n_c;
  type = (int) type_c;
  chunkSize = (int) chunkSize_c;

  As = new double * [5];
  Xs = new double * [5];
  Ys = new double * [5];

  As[ 0 ] = (double *) A01_c; As[ 1 ] = (double *) A02_c; As[ 2 ] = (double *) A03_c;
//  As[ 3 ] = (double *) A04_c; As[ 4 ] = (double *) A05_c;
  Xs[ 0 ] = (double *) x01_c; Xs[ 1 ] = (double *) x02_c; Xs[ 2 ] = (double *) x03_c;
//  Xs[ 3 ] = (double *) x04_c; Xs[ 4 ] = (double *) x05_c;
  Ys[ 0 ] = (double *) y01_c; Ys[ 1 ] = (double *) y02_c; Ys[ 2 ] = (double *) y03_c;
//  Ys[ 3 ] = (double *) y04_c; Ys[ 4 ] = (double *) y05_c; 

#pragma omp parallel num_threads( chunkSize )
{

  double * myX;
  double * myY;
  double * myA;
  long one = 1;

  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < chunkSize; ++i ){
    myX = Xs[ i ];
    myY = Ys[ i ];
    myA = As[ i ];
 
    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break; 
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, myA, myX, one, 0.0, myY, one );
        break;
    }
  }
}

  delete [] As;
  delete [] Xs;
  delete [] Ys;

}


HSTREAMS_EXPORT
void test_gemv_pack_sink(
  uint64_t n_c,
  uint64_t n_mat_c,
  uint64_t type_c,
  uint64_t Apack_c, 
  uint64_t xpack_c,
  uint64_t ypack_c
  ){

  long n, n_mat;
  int type;
  double * Apack, * xpack, * ypack;

  n = (long) n_c;
  n_mat = (long) n_mat_c;
  type = (int) type_c;
  Apack = (double *) Apack_c;
  xpack = (double *) xpack_c;
  ypack = (double *) ypack_c;

#pragma omp parallel
{

  double * myX;
  double * myY;
  double * myA;
  long one = 1;

  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myX = xpack + i * n;
    myY = ypack + i * n;
    myA = Apack + i * Asize;

    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break; 
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, myA, myX, one, 0.0, myY, one );
        break;
    }
  }
}
}

//! prints vetor
void print_vector(
  long n,
  double * y 
  ){
  
  for( long i = 0; i < n; ++i ){
    std::cout << y[ i ] << " ";
  }
  std::cout << std::endl;
}

