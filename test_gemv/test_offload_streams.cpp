/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

// -- includes --
#pragma offload_attribute( push, target( mic ) )
#include <iostream>
#include <fstream>
#include <omp.h>
#include <cstring>
#include <mkl.h>
#include <sys/time.h>
#include <time.h>
#pragma offload_attribute( pop )
#include <offload.h>
#include "aux.h"

// -- function headings -- 
void xfer_to_mic_streams( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr,
  _Offload_stream ** streamHandle = nullptr, int streams_per_domain = 1);

void iter_cpu_mic_stream( int n_iter, long n, long n_mat, int type, 
  double * Apack, double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr,
  _Offload_stream ** streamHandle = nullptr, int streams_per_domain = 1 );

void mv_cpu_mic_streams( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr,
  _Offload_stream ** streamHandle = nullptr, int streams_per_domain = 1 );

void dealloc_on_mic( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void fini_mic_streams( _Offload_stream ** streamHandle, int streams_per_domain );

#define N_MICS ( 2 )

//! main function
int main( 
  int argc, 
  char ** argv
  ){

  if( argc < 6 ){
    //! function parameters
    std::cout << "-test_gemv type mat_size n_mat n_iter n_streams_per_dom" << std::endl;
    return 0;
  }
  
  int type = atoi( argv[ 1 ] );
  long n = atoi( argv[ 2 ] );
  long n_mat = atoi( argv[ 3 ] );
  int n_iter = atoi( argv[ 4 ] );
  int streams_per_domain = atoi( argv[ 5 ] );
  //int chunkSize = atoi( argv[ 7 ]);
  int chunkSize = 5;

  std::cout << "type: ";
  if( type == 0 )
    std::cout << "gemv, ";
  if( type == 1 )
    std::cout << "symv, ";
  if( type == 2 )
    std::cout << "spmv, ";
  std::cout << "n: " << n << ", n_mat: " << n_mat << ", n_iter: " << 
    n_iter << ", streams_per_domain: " << streams_per_domain << std::endl;

  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;
  double * A = new double[ Asize ];
  double * x = new double[ n ];
  double * y = new double[ n ];
  memset( y, 0.0, n * sizeof( double ) );

  //generate_data( n, type, A, x );
  //print_vector( Asize, A );
  //print_vector( n, x );
  //print_vector( n, y );

  double * Apack = new double[ n_mat * Asize ];
  double * xpack = new double[ n_mat * n ];
  double * ypack = new double[ n_mat * n ];

  double * micApack [ N_MICS ];
  double * micXpack [ N_MICS ];
  double * micYpack [ N_MICS ];

  _Offload_stream * streamHandle [ N_MICS ]; 

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  
  generate_data( n, n_mat, type, Apack, xpack );


//! comment/uncomment and run individual test cases:

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs individual offload using streams version
  iter_cpu_mic_stream( n_iter, n, n_mat, type, Apack, xpack, ypack, 
    micApack, micXpack, micYpack, streamHandle, streams_per_domain );
  //print_vector( n * n_mat , ypack );

  delete [] A;
  delete [] x;
  delete [] y;
  delete [] Apack;
  delete [] xpack;
  delete [] ypack;

  return 0;
}

//! individual offload using streams
void iter_cpu_mic_stream(
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack,
  _Offload_stream ** streamHandle,
  int streams_per_domain
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "offload-streams cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "offload-streams cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "offload-streams cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  xfer_to_mic_streams( n, n_mat, type, Apack, xpack, ypack, 
    micApack, micXpack, micYpack, streamHandle, streams_per_domain );
  
  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_cpu_mic_streams( n, n_mat, type, Apack, xpack, ypack, 
      micApack, micXpack, micYpack, streamHandle, streams_per_domain );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_cpu_mic_streams( n, n_mat, type, Apack, xpack, ypack, 
      micApack, micXpack, micYpack, streamHandle, streams_per_domain );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  
  gettimeofday( &t2, nullptr );

  dealloc_on_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
    micYpack );
  fini_mic_streams( streamHandle, streams_per_domain );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;
  
  switch( type ){
    case 0:
      std::cout << "offload-streams cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "offload-streams cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "offload-streams cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );
}

//! transfers individual data to mic using offload streams, allocates buffers
void xfer_to_mic_streams(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack,
  _Offload_stream ** streamHandle,
  int streams_per_domain
  ){
   
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ];
  long offset [ n_mat ];
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  
  for( long i = 0; i < n_mat; ++i ){
    offset[ i ] = i / N_MICS;
  }

  //! streams initiation
  for( int i = 0; i < N_MICS; ++i ){
    streamHandle[ i ] = new _Offload_stream[ streams_per_domain ];
    
    for( int j = 0; j < streams_per_domain; ++j ){
      streamHandle[ i ][ j ] = 
        
      //_Offload_stream_create( i, 240 / streams_per_domain );
      _Offload_stream_create( i, 1 );
    
    }
  }

#pragma omp parallel
{

  int sizeA;
  int sizeX;
  int sizeY;
  double * A_p = nullptr;
  double * X_p = nullptr;
  double * Y_p = nullptr;

  //__attribute__((target( mic ))) double * A_p;


#pragma omp for //schedule( static, 1 ) 
  for( int i = 0; i < N_MICS; ++i ){

    sizeA = size[ i ] * Asize;
    sizeX = size[ i ] * n;
    sizeY = size[ i ] * n;
    
// Allocate memory on MICs
#pragma offload_transfer target( mic : i ) \
nocopy( A_p : length( sizeA ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( X_p : length( sizeX ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( Y_p : length( sizeY ) alloc_if( 1 ) free_if( 0 ) targetptr )

    micApack[ i ] = A_p;
    micXpack[ i ] = X_p;
    micYpack[ i ] = Y_p;

  }

  double * myA;
  double * micApack_p;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myA = Apack + i * Asize;
    micApack_p = micApack[ i % N_MICS ] + ( offset[ i ] * Asize );

// Transfer individual matrices from CPU to specific MIC, fit into the pre-allocated buffer
#pragma offload_transfer target( mic : ( i % N_MICS ) ) \
in( myA : length( Asize ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr )

  }

}

}


//! performs matrix-vector multiplication using individual offload streams
void mv_cpu_mic_streams( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack,
  _Offload_stream ** streamHandle,
  int streams_per_domain
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ];
  long offset [ n_mat ];
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  
  for( long i = 0; i < n_mat; ++i ){
    offset[ i ] = i / N_MICS;
  }

#pragma omp parallel
{
  double * myX_cpu;
  double * myY_cpu;
  double * myA_cpu;
  double * micApack_p;
  double * micXpack_p;
  double * micYpack_p;
  long one = 1;

  _Offload_stream sHandle;

#pragma omp for //schedule( dynamic, 24 )
  for( long i = 0; i < n_mat; ++i ){
    myX_cpu = xpack + i * n;
    myY_cpu = ypack + i * n;
    myA_cpu = Apack + i * Asize;

    micApack_p = micApack[ i % N_MICS ] + ( offset[ i ] * Asize );
    micXpack_p = micXpack[ i % N_MICS ] + ( offset[ i ] * n );
    micYpack_p = micYpack[ i % N_MICS ] + ( offset[ i ] * n );

    sHandle = streamHandle[ i % N_MICS ][ ( i / N_MICS ) % streams_per_domain ];

#pragma offload stream( sHandle ) target( mic ) \
in( myA_cpu : length( 0 ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myX_cpu : length( n ) into( micXpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myY_cpu : length( n ) into( micYpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr )
{
    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, micApack_p, n, 
          micXpack_p, one, 0.0, micYpack_p, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, micApack_p, n, 
          micXpack_p, one, 0.0, micYpack_p, one );
        break;
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, micApack_p, 
          micXpack_p, one, 0.0, micYpack_p, one );
        break;
     }
}

#pragma offload_transfer stream( sHandle ) target( mic ) \
out( micYpack_p : length( n ) into( myY_cpu ) alloc_if( 0 ) free_if( 0 ) targetptr )


  }
}

#pragma offload_wait stream( 0 ) target( mic )
 
}

//! deallocates individual buffers on mic
void dealloc_on_mic(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){
     
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp parallel num_threads( N_MICS )
{

  double * A_p;
  double * X_p;
  double * Y_p;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < N_MICS; ++i ){

  A_p = micApack[ i ];
  X_p = micXpack[ i ];
  Y_p = micYpack[ i ];

#pragma offload_transfer target( mic : i ) \
in( A_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr ) \
in( X_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr ) \
in( Y_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr )
  }
}

}

//! finalizes offload streams, destroys streams
void fini_mic_streams(
  _Offload_stream ** streamHandle,
  int streams_per_domain
  ){

  for( int i = 0; i < N_MICS; ++i ){

    for( int j = 0; j < streams_per_domain; ++j ){
      _Offload_stream_destroy( i, streamHandle[ i ][ j ] );
    }
  }

  for( int i = 0; i < N_MICS; ++i )
    delete [] streamHandle[ i ];

}


