/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

// -- includes --
#pragma offload_attribute( push, target( mic ) )
#include <iostream>
#include <fstream>
#include <omp.h>
#include <cstring>
#include <mkl.h>
#include <sys/time.h>
#include <time.h>
#pragma offload_attribute( pop )
#include <hStreams_app_api.h>
#include <hStreams_source.h>
#include <offload.h>
#include "aux.h"

// -- function headings -- 
HSTR_RESULT init_hstreams( int streams_per_domain, int oversubscription, 
  long n, long n_mat, int type, double * Apack, double * xpack, double * ypack );

HSTR_RESULT init_hstreams_chunked( int streams_per_domain, 
  int oversubscription, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, int chunkSize );

HSTR_RESULT init_hstreams_targetptr( int streams_per_domain, int oversubscription, 
  long n, long n_mat, int type, double * Apack, double * xpack, double * ypack );

HSTR_RESULT init_hstreams_pack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

HSTR_RESULT fini_hstreams_pack( long n, long n_mat, int type, 
  double * Apack, double * xpack, double * ypack );

HSTR_RESULT fini_hstreams( int streams_per_domain, int oversubscription, long n, 
  long n_mat, int type, double * Apack, double * xpack, double * ypack );

HSTR_RESULT fini_hstreams_chunked( int streams_per_domain, 
  int oversubscription, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, int chunkSize );

HSTR_RESULT iter_hstreams_pack( int n_iter, long n, long n_mat, int type, 
  double * Apack, double * xpack, double * ypack );

HSTR_RESULT iter_hstreams_targetptr( int streams_per_domain, int oversubscription, 
  int n_iter, long n, long n_mat, int type, double * Apack, double * xpack, 
  double * ypack );

HSTR_RESULT iter_hstreams( int streams_per_domain, int oversubscription, 
  int n_iter, long n, long n_mat, int type, double * Apack, double * xpack, 
  double * ypack );

HSTR_RESULT iter_hstreams_chunked( int streams_per_domain, int oversubscription, 
  int n_iter, long n, long n_mat, int type, double * Apack, double * xpack, 
  double * ypack, int chunkSize );

HSTR_RESULT mv_hstreams_pack( long n, long n_mat, int type, 
  double * Apack, double * xpack, double * ypack );

HSTR_RESULT mv_hstreams_targetptr( int streams_per_domain, int oversubscription, long n, 
  long n_mat, int type, double * Apack, double * xpack, double * ypack );

HSTR_RESULT mv_hstreams( int streams_per_domain, int oversubscription, long n, 
  long n_mat, int type, double * Apack, double * xpack, double * ypack );

HSTR_RESULT mv_hstreams_chunked( int streams_per_domain, int oversubscription, 
  long n, long n_mat, int type, double * Apack, double * xpack, double * ypack,
  int chunkSize  );

#define N_MICS ( 2 )


//! main function
int main( 
  int argc, 
  char ** argv
  ){

  if( argc < 7 ){
    //! function parameters
    std::cout << "-test_gemv type mat_size n_mat n_iter n_streams_per_dom oversubscription" << std::endl;
    return 0;
  }
  
  int type = atoi( argv[ 1 ] );
  long n = atoi( argv[ 2 ] );
  long n_mat = atoi( argv[ 3 ] );
  int n_iter = atoi( argv[ 4 ] );
  int streams_per_domain = atoi( argv[ 5 ] );
  int oversubscription = atoi( argv[ 6 ] );
  //int chunkSize = atoi( argv[ 7 ]);
  int chunkSize = 5;

  std::cout << "type: ";
  if( type == 0 )
    std::cout << "gemv, ";
  if( type == 1 )
    std::cout << "symv, ";
  if( type == 2 )
    std::cout << "spmv, ";
  std::cout << "n: " << n << ", n_mat: " << n_mat << ", n_iter: " << 
    n_iter << ", streams_per_domain: " << streams_per_domain << 
    ", oversubscription: " << oversubscription << std::endl;


  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;
  double * A = new double[ Asize ];
  double * x = new double[ n ];
  double * y = new double[ n ];
  memset( y, 0.0, n * sizeof( double ) );

  //generate_data( n, type, A, x );
  //print_vector( Asize, A );
  //print_vector( n, x );
  //print_vector( n, y );

  double * Apack = new double[ n_mat * Asize ];
  double * xpack = new double[ n_mat * n ];
  double * ypack = new double[ n_mat * n ];

  double * micApack [ N_MICS ];
  double * micXpack [ N_MICS ];
  double * micYpack [ N_MICS ];


  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  
  generate_data( n, n_mat, type, Apack, xpack );


  //! comment/uncomment and run individual test cases:

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs hStream pack version
  iter_hstreams_pack( n_iter, n, n_mat, type, Apack, xpack, ypack );
  //print_vector( n * n_mat , ypack );
 
  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs individual hStream simulating targetr-pointer version
  iter_hstreams_targetptr( streams_per_domain, oversubscription, n_iter, n, n_mat, 
    type, Apack, xpack, ypack );
  //print_vector( n * n_mat , ypack );

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs chunked hStream version
  //!! THIS ONE DOES NOT WORK CORRECTLY - DO NOT CALL IT 
  //iter_hstreams_chunked( streams_per_domain, oversubscription, n_iter, n, n_mat, 
  //  type, Apack, xpack, ypack, chunkSize ); 
  //print_vector( n * n_mat, ypack );

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs individual hStream version
  iter_hstreams( streams_per_domain, oversubscription, n_iter, n, n_mat, 
    type, Apack, xpack, ypack );
  //print_vector( n * n_mat , ypack );  

  delete [] A;
  delete [] x;
  delete [] y;
  delete [] Apack;
  delete [] xpack;
  delete [] ypack;

  return 0;
}


//! hStreams pack
HSTR_RESULT iter_hstreams_pack(
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "hstreams-packed cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "hstreams-packed cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "hstreams-packed cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  init_hstreams_pack( n, n_mat, type, Apack, xpack, ypack );
  
  gettimeofday( &t1, nullptr );
  
  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_hstreams_pack( n, n_mat, type, Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_hstreams_pack( n, n_mat, type, Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  gettimeofday( &t2, nullptr );
  fini_hstreams_pack( n, n_mat, type, Apack, xpack, ypack );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t =  ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "hstreams-packed cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "hstreams-packed cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "hstreams-packed cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );

  return HSTR_RESULT_SUCCESS;

}

//! individual hStreams simulating target-pointer
HSTR_RESULT iter_hstreams_targetptr(
  int streams_per_domain,
  int oversubscription,
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "hstreams_targetptr cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "hstreams_targetptr cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "hstreams_targetptr cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  init_hstreams_targetptr( streams_per_domain, oversubscription, n, n_mat, type, 
    Apack, xpack, ypack );

  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_hstreams_targetptr( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/  
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_hstreams_targetptr( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }

  gettimeofday( &t2, nullptr );
  
  fini_hstreams_pack( n, n_mat, type, 
    Apack, xpack, ypack );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "hstreams_targetptr cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "hstreams_targetptr cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "hstreams_targetptr cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );

  return HSTR_RESULT_SUCCESS;

}

//! chunked hStreams
HSTR_RESULT iter_hstreams_chunked(
  int streams_per_domain,
  int oversubscription,
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  int chunkSize
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "chunked-hstreams cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "chunked-hstreams cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "chunked-hstreams cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  init_hstreams_chunked( streams_per_domain, oversubscription, n, n_mat, type, 
    Apack, xpack, ypack, chunkSize );

  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
/*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_hstreams_chunked( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack, chunkSize );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
*/ 
 
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_hstreams_chunked( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack, chunkSize );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }

  gettimeofday( &t2, nullptr );
  
  fini_hstreams_chunked( streams_per_domain, oversubscription, n, n_mat, type, 
    Apack, xpack, ypack, chunkSize );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "chunked-hstreams cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "chunked-hstreams cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "chunked-hstreams cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );

  return HSTR_RESULT_SUCCESS;

}

//! individual hStreams
HSTR_RESULT iter_hstreams(
  int streams_per_domain,
  int oversubscription,
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "hstreams cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "hstreams cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "hstreams cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  init_hstreams( streams_per_domain, oversubscription, n, n_mat, type, 
    Apack, xpack, ypack );

  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_hstreams( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/  
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_hstreams( streams_per_domain, oversubscription, n, n_mat, type, 
      Apack, xpack, ypack );
    CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }

  gettimeofday( &t2, nullptr );
  
  fini_hstreams( streams_per_domain, oversubscription, n, n_mat, type, 
    Apack, xpack, ypack );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "hstreams cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "hstreams cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "hstreams cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );

  return HSTR_RESULT_SUCCESS;

}

//! perfomrs a matrix-vector multiplication: hStreams pack
HSTR_RESULT mv_hstreams_pack( 
  long n, 
  long n_mat, 
  int type, // 0 ... gemv, 1 ... symv, 2 ... spmv 
  double * Apack, 
  double * xpack, 
  double * ypack
  ){

  uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ nDomains ]; 
  long offset [ nDomains ];
  long sizeall = n_mat / nDomains;
  for( int i = 0; i < nDomains; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % nDomains;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < nDomains; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];
 
#pragma omp parallel for num_threads( nDomains ) schedule( static, 1 )
  for( int i = 0; i < nDomains; ++i ){

    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

    hStreams_app_xfer_memory( 
      i, subxpack, subxpack, n * size[ i ] * sizeof( double ), 
      HSTR_SRC_TO_SINK, nullptr );
    hStreams_app_xfer_memory( 
      i, subypack, subypack, n * size[ i ] * sizeof( double ), 
      HSTR_SRC_TO_SINK, nullptr );

    uint64_t args[ 6 ];
    args[ 0 ] = (uint64_t) n;
    args[ 1 ] = (uint64_t) size[ i ];
    args[ 2 ] = (uint64_t) type;
    args[ 3 ] = (uint64_t) subApack;
    args[ 4 ] = (uint64_t) subxpack;
    args[ 5 ] = (uint64_t) subypack;
   
    hStreams_app_invoke( 
      i, "test_gemv_pack_sink", 3, 3, args, nullptr, nullptr, 0 );

    hStreams_app_xfer_memory( 
      i, subypack, subypack, n * size[ i ] * sizeof( double ), 
      HSTR_SINK_TO_SRC, nullptr );

  }
  
  return HSTR_RESULT_SUCCESS;
}

//! perfomrs a matrix-vector multiplication: individual hStreams simulating target-pointer
HSTR_RESULT mv_hstreams_targetptr( 
  int streams_per_domain,
  int oversubscription,
  long n, 
  long n_mat, 
  int type, // 0 ... gemv, 1 ... symv, 2 ... spmv 
  double * Apack, 
  double * xpack, 
  double * ypack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( &CardsFound, &nDomains, &homog ) );

  long size [ nDomains ]; 
  long offset [ nDomains ];
  long sizeall = n_mat / nDomains;
  for( int i = 0; i < nDomains; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % nDomains;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < nDomains; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

  uint32_t numLogStreams[ nDomains ];

  HSTR_LOG_STR * LogStreamsIDs[ nDomains ]; 
  
  for( int i = 0; i < nDomains; ++i ) {
    CHECK_HSTR_RESULT( hStreams_GetNumLogStreams ( 
      i+1, &numLogStreams[i] ) );
    LogStreamsIDs[ i ] = new HSTR_LOG_STR[ numLogStreams[ i ] ];
    CHECK_HSTR_RESULT( hStreams_GetLogStreamIDList( 
      i+1, numLogStreams[i], LogStreamsIDs[i] ) );
  }


//! sends vectors: one per mic in round-robin 
#pragma omp parallel
{

  double * myA;
  double * myX;
  double * myY;
  uint32_t streamID;

#pragma omp for
  for( long i = 0; i < size[ nDomains - 1 ]; ++i ){  
    for( uint32_t targetLogDomain = 1; targetLogDomain <= nDomains; ++targetLogDomain ){
      myA = Apack + ( offset[ targetLogDomain - 1 ] + i ) * Asize;
      myX = xpack + ( offset[ targetLogDomain - 1 ] + i ) * n;
      myY = ypack + ( offset[ targetLogDomain - 1 ] + i ) * n;

      // matrix to hstreamID mapping (zero-based)
      streamID = LogStreamsIDs[ targetLogDomain - 1 ][ i % numLogStreams[ targetLogDomain - 1 ] ];
      // matrix to logical domain mapping (one-based)
    
      hStreams_app_xfer_memory( 
        streamID, myX, myX, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr ); 

      uint64_t args[ 5 ];
      args[ 0 ] = (uint64_t) n;
      args[ 1 ] = (uint64_t) type;
      args[ 2 ] = (uint64_t) myA;
      args[ 3 ] = (uint64_t) myX;
      args[ 4 ] = (uint64_t) myY;

      // invoke sink function
      hStreams_app_invoke( streamID, "test_gemv_sink", 2, 3, args, 
        nullptr, nullptr, 0 );
    
      // send result back to CPU   
      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SINK_TO_SRC, nullptr );
    
    }
  }

#pragma omp for
  for( uint32_t targetLogDomain = 1; targetLogDomain <= n_mat % nDomains ; ++targetLogDomain ){
      myA = Apack + ( offset[ targetLogDomain - 1 ] + size[ 0 ] - 1 ) * Asize;
      myX = xpack + ( offset[ targetLogDomain - 1 ] + size[ 0 ] - 1 ) * n;
      myY = ypack + ( offset[ targetLogDomain - 1 ] + size[ 0 ] - 1 ) * n;

      // matrix to hstreamID mapping (zero-based)
      streamID = LogStreamsIDs[ targetLogDomain - 1 ][ size[ 0 ] % numLogStreams[ targetLogDomain - 1 ] ];
      // matrix to logical domain mapping (one-based)
    
      hStreams_app_xfer_memory( 
        streamID, myX, myX, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr ); 

      uint64_t args[ 5 ];
      args[ 0 ] = (uint64_t) n;
      args[ 1 ] = (uint64_t) type;
      args[ 2 ] = (uint64_t) myA;
      args[ 3 ] = (uint64_t) myX;
      args[ 4 ] = (uint64_t) myY;

      // invoke sink function
      hStreams_app_invoke( streamID, "test_gemv_sink", 2, 3, args, 
        nullptr, nullptr, 0 );
    
      // send result back to CPU   
      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SINK_TO_SRC, nullptr );
 
    }
}

  for( int i = 0; i < nDomains; ++i )
    delete[] LogStreamsIDs[ i ];

  return HSTR_RESULT_SUCCESS;
  
}

//! perfomrs a matrix-vector multiplication: chunked hStreams
HSTR_RESULT mv_hstreams_chunked( 
  int streams_per_domain,
  int oversubscription,
  long n, 
  long n_mat, 
  int type, // 0 ... gemv, 1 ... symv, 2 ... spmv 
  double * Apack, 
  double * xpack, 
  double * ypack,
  int chunkSize
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( &CardsFound, &nDomains, &homog ) );

  std::cout << "chunkSize = " << chunkSize << std::endl;

#pragma omp parallel
{
  
  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat/chunkSize; ++i ){
  
    // matrix to hstreamID mapping (zero-based)
    streamID = i % ( streams_per_domain * oversubscription * nDomains );

    uint64_t args[ 3 + 3 * 3 ];
    args[ 0 ] = (uint64_t) n;
    args[ 1 ] = (uint64_t) type;
    args[ 2 ] = (uint64_t) chunkSize;

    for( int j = 0; j < chunkSize; ++j ){

      myA = Apack + i * Asize * chunkSize + j * Asize;
      myX = xpack + i * n * chunkSize + j * n;
      myY = ypack + i * n * chunkSize + j * n;

      args[ 3 + 3 * j ] = (uint64_t) myA;
      args[ 3 + 3 * j + 1 ] = (uint64_t) myX;
      args[ 3 + 3 * j + 2 ] = (uint64_t) myY;

      hStreams_app_xfer_memory( 
        streamID, myX, myX, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );

      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
    }
 
    // invoke sink function
    hStreams_app_invoke( streamID, "test_gemv_chunked_sink", 3, 3 * 3, args, 
      nullptr, nullptr, 0 );

    for( int j = 0; j < chunkSize; ++j ){

      myY = ypack + i * n * chunkSize + j * n;
    
      hStreams_app_xfer_memory( 
        streamID, myY, myY, n * sizeof( double ), HSTR_SINK_TO_SRC, nullptr );  
    }

  }

if( n_mat % chunkSize ){
#pragma omp single
{
  targetLogDomain = 1;
  streamID = n_mat/chunkSize % ( oversubscription * streams_per_domain * nDomains );

  uint64_t args[ 3 + 3 * 3 ];
  args[ 0 ] = (uint64_t) n;
  args[ 1 ] = (uint64_t) type;
  args[ 2 ] = (uint64_t) chunkSize;

  for( long j = 0; j < n_mat % chunkSize; ++j ){

    myX = xpack + n_mat/chunkSize * chunkSize * n + j * n;
    myY = ypack + n_mat/chunkSize * chunkSize * n + j * n;
    myA = Apack + n_mat/chunkSize * chunkSize * Asize + j * Asize;

    args[ 3 + 3 * j ] = (uint64_t) myA;
    args[ 3 + 3 * j + 1 ] = (uint64_t) myX;
    args[ 3 + 3 * j + 2 ] = (uint64_t) myY;

    hStreams_app_xfer_memory( 
      streamID, myX, myX, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );
    
    hStreams_app_xfer_memory( 
      streamID, myY, myY, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
  }

  // invoke sink function
  hStreams_app_invoke( streamID, "test_gemv_chunked_sink", 3, 3 * 3 , args, 
    nullptr, nullptr, 0 );

  for( long j = 0; j < n_mat % chunkSize; ++j ){

    myY = ypack + n_mat/chunkSize * n * chunkSize + j * n;
    
    hStreams_app_xfer_memory( 
      streamID, myY, myY, n * sizeof( double ), HSTR_SINK_TO_SRC, nullptr );  
    }

}
}


}

  return HSTR_RESULT_SUCCESS;
}

//! perfomrs a matrix-vector multiplication: individual hStreams
HSTR_RESULT mv_hstreams( 
  int streams_per_domain,
  int oversubscription,
  long n, 
  long n_mat, 
  int type, // 0 ... gemv, 1 ... symv, 2 ... spmv 
  double * Apack, 
  double * xpack, 
  double * ypack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( &CardsFound, &nDomains, &homog ) );

#pragma omp parallel
{
  
  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myX = xpack + i * n;
    myY = ypack + i * n;
    myA = Apack + i * Asize;

    // matrix to hstreamID mapping (zero-based)
    streamID = i % ( streams_per_domain * oversubscription * nDomains );
    
    hStreams_app_xfer_memory( 
      streamID, myX, myX, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );
    
    hStreams_app_xfer_memory( 
      streamID, myY, myY, n * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
    
    uint64_t args[ 5 ];
    args[ 0 ] = (uint64_t) n;
      args[ 1 ] = (uint64_t) type;
    args[ 2 ] = (uint64_t) myA;
    args[ 3 ] = (uint64_t) myX;
    args[ 4 ] = (uint64_t) myY;

    // invoke sink function
    hStreams_app_invoke( streamID, "test_gemv_sink", 2, 3, args, 
      nullptr, nullptr, 0 );
    
    // send result back to CPU   
    hStreams_app_xfer_memory( 
      streamID, myY, myY, n * sizeof( double ), HSTR_SINK_TO_SRC, nullptr );
    
  }
}

  return HSTR_RESULT_SUCCESS;
}

//! initiates hStreams, sends data to mics
HSTR_RESULT init_hstreams_pack(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  // packed version has 1 hStream per mic
  int streams_per_domain = 1;
  int oversubscription = 1;
  
  CHECK_HSTR_RESULT( hStreams_app_init( 
    streams_per_domain, oversubscription ) );
   uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );
 
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ nDomains ]; 
  long offset [ nDomains ];
  long sizeall = n_mat / nDomains;
  for( int i = 0; i < nDomains; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % nDomains;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < nDomains; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

#pragma omp parallel num_threads( nDomains )
{
  uint32_t targetLogDomain;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < nDomains; ++i ){

    targetLogDomain = i + 1;    

    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

    hStreams_Alloc1DEx( 
      (void *) subApack, Asize * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) subxpack, n * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) subypack, n * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
    hStreams_app_xfer_memory( 
      i, subApack, subApack, Asize * size[ i ] * sizeof( double ), 
      HSTR_SRC_TO_SINK, nullptr );  

}
}
  return HSTR_RESULT_SUCCESS;
}

HSTR_RESULT init_hstreams_targetptr(
  int streams_per_domain,
  int oversubscription,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){
  
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;
  
  CHECK_HSTR_RESULT( hStreams_app_init( 
    streams_per_domain, oversubscription ) );

  uint32_t CardsFound, nDomains, nLogDomains;
  bool homog;

  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

  long size [ nDomains ]; 
  long offset [ nDomains ];
  long sizeall = n_mat / nDomains;
  for( int i = 0; i < nDomains; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % nDomains;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < nDomains; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

  uint32_t numLogStreams[ nDomains ];

  HSTR_LOG_STR * LogStreamsIDs[ nDomains ]; 
  
  for( int i = 0; i < nDomains; ++i ) {
    CHECK_HSTR_RESULT( hStreams_GetNumLogStreams ( 
      i+1, &numLogStreams[i] ) );
    LogStreamsIDs[ i ] = new HSTR_LOG_STR[ numLogStreams[ i ] ];
    CHECK_HSTR_RESULT( hStreams_GetLogStreamIDList( 
      i+1, numLogStreams[i], LogStreamsIDs[i] ) );
  }

#pragma omp parallel num_threads( nDomains )
{
  uint32_t targetLogDomain;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < nDomains; ++i ){

    targetLogDomain = i + 1;    

    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

    hStreams_Alloc1DEx( 
      (void *) subApack, Asize * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) subxpack, n * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) subypack, n * size[ i ] * sizeof( double ), nullptr, 1, 
      &targetLogDomain );
}
}


//! sends matrices: one per mic in round-robin 
#pragma omp parallel
{

  double * myA;
  uint32_t streamID;

#pragma omp for
  for( long i = 0; i < size[ nDomains - 1 ]; ++i ){  
    for( uint32_t targetLogDomain = 1; targetLogDomain <= nDomains; ++targetLogDomain ){
      myA = Apack + ( offset[ targetLogDomain - 1 ] + i ) * Asize;
  
      // matrix to hstreamID mapping (zero-based)
      streamID = LogStreamsIDs[ targetLogDomain - 1 ][ i % numLogStreams[ targetLogDomain - 1 ] ];
      // matrix to logical domain mapping (one-based)
    
      hStreams_app_xfer_memory( 
        streamID, myA, myA, Asize * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  

    }
  }

#pragma omp for
  for( uint32_t targetLogDomain = 1; targetLogDomain <= n_mat % nDomains ; ++targetLogDomain ){
      myA = Apack + ( offset[ targetLogDomain - 1 ] + size[ 0 ] - 1 ) * Asize;
  
      // matrix to hstreamID mapping (zero-based)
      streamID = LogStreamsIDs[ targetLogDomain - 1 ][ size[ 0 ] % numLogStreams[ targetLogDomain - 1 ] ];
      // matrix to logical domain mapping (one-based)
    
      hStreams_app_xfer_memory( 
        streamID, myA, myA, Asize * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
    }

}

/*
//! sends matrices: all per individual mic
for( uint32_t targetLogDomain = 1; targetLogDomain <= nDomains; ++targetLogDomain ){

#pragma omp parallel
{

  double * myA;

  uint32_t streamID;

#pragma omp for
  for( int i = 0; i < size[ targetLogDomain - 1 ]; ++i ){
    myA = Apack + ( offset[ targetLogDomain - 1 ] + i ) * Asize;


    // matrix to hstreamID mapping (zero-based)
    streamID = LogStreamsIDs[ targetLogDomain - 1 ][ i % numLogStreams[ targetLogDomain - 1 ] ];
    // matrix to logical domain mapping (one-based)
    
    hStreams_app_xfer_memory( 
      streamID, myA, myA, Asize * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
  }
}

}
*/
  for( int i = 0; i < nDomains; ++i )
    delete[] LogStreamsIDs[ i ];

  return HSTR_RESULT_SUCCESS;
}

//! initiates chunked hStreams, sends data to mic
HSTR_RESULT init_hstreams_chunked(
  int streams_per_domain,
  int oversubscription,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  int chunkSize
  ){
  
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;
  
  CHECK_HSTR_RESULT( hStreams_app_init( 
    streams_per_domain, oversubscription ) );

  uint32_t CardsFound, nDomains, nLogDomains;
  bool homog;

  CHECK_HSTR_RESULT( hStreams_GetNumLogDomains( HSTR_SRC_PHYS_DOMAIN, &nLogDomains ));

  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

#pragma omp parallel
{

  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat/chunkSize; ++i ){
  
    // matrix to hstreamID mapping (zero-based)
    streamID = i % ( streams_per_domain * oversubscription * nDomains );
    // matrix to logical domain mapping (one-based)
    targetLogDomain = 
      ( i % ( nDomains * streams_per_domain ) ) / streams_per_domain + 1;

    for( int j = 0; j < chunkSize; ++j ){

      myX = xpack + i * n * chunkSize + j * n;
      myY = ypack + i * n * chunkSize + j * n;
      myA = Apack + i * Asize * chunkSize + j * Asize;
       
      hStreams_Alloc1DEx( 
        (void *) myA, Asize * sizeof( double ), nullptr, 1, &targetLogDomain );
      hStreams_Alloc1DEx( 
        (void *) myX, n *  sizeof( double ), nullptr, 1, &targetLogDomain );
      hStreams_Alloc1DEx( 
        (void *) myY, n  * sizeof( double ), nullptr, 1, &targetLogDomain );
      hStreams_app_xfer_memory( 
        streamID, myA, myA, Asize  * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
    }
  }

#pragma omp single
{
  targetLogDomain = 1;
  streamID = n_mat/chunkSize % ( oversubscription * streams_per_domain * nDomains );

  for( long j = 0; j < n_mat % chunkSize; ++j ){

    myX = xpack + n_mat/chunkSize * chunkSize * n + j * n;
    myY = ypack + n_mat/chunkSize * chunkSize * n + j * n;
    myA = Apack + n_mat/chunkSize * chunkSize * Asize + j * Asize;

    hStreams_Alloc1DEx( 
      (void *) myA, Asize  * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) myX, n * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) myY, n * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_app_xfer_memory( 
      streamID, myA, myA, Asize * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
  }
}

}

  return HSTR_RESULT_SUCCESS;
}

//! initiates individual hStreams, sends data to mic
HSTR_RESULT init_hstreams(
  int streams_per_domain,
  int oversubscription,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){
  
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;
  
  CHECK_HSTR_RESULT( hStreams_app_init( 
    streams_per_domain, oversubscription ) );

  uint32_t CardsFound, nDomains, nLogDomains;
  bool homog;

  CHECK_HSTR_RESULT( hStreams_GetNumLogDomains( HSTR_SRC_PHYS_DOMAIN, &nLogDomains ));

  std::cout << "numLogDomains = " << nLogDomains << std::endl;

  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

  std::cout << "CardsFound = " << CardsFound << ", NumActive = " << nDomains << std::endl;

#pragma omp parallel
{

  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myX = xpack + i * n;
    myY = ypack + i * n;
    myA = Apack + i * Asize;

    // matrix to hstreamID mapping (zero-based)
    streamID = i % ( streams_per_domain * oversubscription * nDomains );
    // matrix to logical domain mapping (one-based)
    targetLogDomain = 
      ( i % ( nDomains * streams_per_domain ) ) / streams_per_domain + 1;
    
    hStreams_Alloc1DEx( 
      (void *) myA, Asize * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) myX, n * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_Alloc1DEx( 
      (void *) myY, n * sizeof( double ), nullptr, 1, &targetLogDomain );
    hStreams_app_xfer_memory( 
      streamID, myA, myA, Asize * sizeof( double ), HSTR_SRC_TO_SINK, nullptr );  
  }

}

  return HSTR_RESULT_SUCCESS;
}

//! finalizes hStreams, deallocates data on mic
HSTR_RESULT fini_hstreams_pack(
  long n,
  long n_mat,
  int type, 
  double * Apack,
  double * xpack,
  double * ypack
  ){
  
  uint32_t CardsFound, nDomains;
  bool homog;
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );
 
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ nDomains ]; 
  long offset [ nDomains ];
  long sizeall = n_mat / nDomains;
  for( int i = 0; i < nDomains; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % nDomains;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < nDomains; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];
  
  uint32_t targetLogDomain;

  for( int i = 0; i < nDomains; ++i ){
    
    targetLogDomain = i + 1;    

    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

    hStreams_RmBufferLogDomains( 
      (void *) subApack, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) subxpack, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) subypack, 1, &targetLogDomain );

}
  
  CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
  
  CHECK_HSTR_RESULT( hStreams_app_fini( ) );

  return HSTR_RESULT_SUCCESS;
}

//! finalizes chunked hStreams, deallocates data on mic
HSTR_RESULT fini_hstreams_chunked(
  int streams_per_domain,
  int oversubscription,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  int chunkSize
  ){

  uint32_t CardsFound, nDomains;
  bool homog;
  
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp parallel
{

  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat/chunkSize; ++i ){
  
    // matrix to logical domain mapping (one-based)
    targetLogDomain = 
      ( i % ( nDomains * streams_per_domain ) ) / streams_per_domain + 1;

    for( int j = 0; j < chunkSize; ++j ){

      myX = xpack + i * n * chunkSize + j * n;
      myY = ypack + i * n * chunkSize + j * n;
      myA = Apack + i * Asize * chunkSize + j * Asize;
       
      hStreams_RmBufferLogDomains( 
        (void *) myA, 1, &targetLogDomain );
      hStreams_RmBufferLogDomains( 
        (void *) myX, 1, &targetLogDomain );
      hStreams_RmBufferLogDomains( 
        (void *) myY, 1, &targetLogDomain );    
      }
  }

#pragma omp single
{
  targetLogDomain = 1;

  for( long j = 0; j < n_mat % chunkSize; ++j ){

    myX = xpack + n_mat/chunkSize * chunkSize * n + j * n;
    myY = ypack + n_mat/chunkSize * chunkSize * n + j * n;
    myA = Apack + n_mat/chunkSize * chunkSize * Asize + j * Asize;

    hStreams_RmBufferLogDomains( 
      (void *) myA, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) myX, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) myY, 1, &targetLogDomain );  

  }
}

}

  CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
  
  CHECK_HSTR_RESULT( hStreams_app_fini( ) );

  return HSTR_RESULT_SUCCESS;
}


//! finalizes individual hStreams, deallocates data on mic
HSTR_RESULT fini_hstreams(
  int streams_per_domain,
  int oversubscription,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  uint32_t CardsFound, nDomains;
  bool homog;
  
  CHECK_HSTR_RESULT( hStreams_GetNumPhysDomains( 
    &CardsFound, &nDomains, &homog ) );

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp parallel
{

  double * myX;
  double * myY;
  double * myA;

  uint32_t streamID, targetLogDomain;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myX = xpack + i * n;
    myY = ypack + i * n;
    myA = Apack + i * Asize;

    // matrix to logical domain mapping (one-based)
    targetLogDomain = ( i % ( nDomains * streams_per_domain ) ) / streams_per_domain + 1;
    
    hStreams_RmBufferLogDomains( 
      (void *) myA, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) myX, 1, &targetLogDomain );
    hStreams_RmBufferLogDomains( 
      (void *) myY, 1, &targetLogDomain );
  }

}

  CHECK_HSTR_RESULT( hStreams_app_thread_sync( ) );
  
  CHECK_HSTR_RESULT( hStreams_app_fini( ) );

  return HSTR_RESULT_SUCCESS;
}

//! finalizes offload streams, destroys streams
void fini_mic_streams(
  _Offload_stream ** streamHandle,
  int streams_per_domain
  ){

  for( int i = 0; i < N_MICS; ++i ){

    for( int j = 0; j < streams_per_domain; ++j ){
      _Offload_stream_destroy( i, streamHandle[ i ][ j ] );
    }
  }

  for( int i = 0; i < N_MICS; ++i )
    delete [] streamHandle[ i ];

}



