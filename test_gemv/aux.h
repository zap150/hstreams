 /*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

#pragma offload_attribute( push, target( mic ) )

void printDelim(){
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
}

//! prints vetor
void print_vector(
  long n,
  double * y 
  ){
  
  for( int i = 0; i < n; ++i ){
    std::cout << y[ i ] << " ";
  }
  std::cout << std::endl;
}

#pragma offload_attribute( pop )

//! generates random matrix and vector data
void generate_data(
  long n,
  int type, // 0,1 ... full, 2 ... packed
  double * A,
  double * x
  ){

  long counter = 0;
  long ind;
  for( long j = 0; j < n; ++j ){
    x[ j ] = ( 7 * j + 3 ) % 11 / 11.0;
    for( long i = j; i < n; ++i ){
      if( type != 2 ){
        ind = i + j * n;
      } else {
        ind = counter;
      }
      A[ ind ] = ( 3 * i + 5 * j + 7 ) % 13 / 13.0;
      if( i != j && type != 2 ){
        A[ j + i * n ] = A[ ind ];
      }
      ++counter;
    }
  }
}

//! generates random matrix and vector data
void generate_data(
  long n,
  long n_mat,
  int type, // 0,1 ... full, 2 ... packed
  double * AA,
  double * xx
  ){

  long counter = 0;
  long ind;
  for( long k = 0; k < n_mat; ++k ){
    for( long j = 0; j < n; ++j ){
      xx[ j + k * n ] = ( 7 * j + 5 * k + 3 ) % 11 / 11.0;
      for( long i = j; i < n; ++i ){
        if( type != 2 ){
          ind = i + j * n + k * n * n;
        } else {
          ind = counter;
        }
        AA[ ind ] = ( 3 * i + 5 * j + 7 * k + 9 ) % 13 / 13.0;
        if( i != j && type != 2 ){
          AA[ j + i * n + k * n * n ] = AA[ ind ];
        }
        ++counter;
      }
    }
  }
}


