/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

// -- includes --
#pragma offload_attribute( push, target( mic ) )
#include <iostream>
#include <fstream>
#include <omp.h>
#include <cstring>
#include <mkl.h>
#include <sys/time.h>
#include <time.h>
#pragma offload_attribute( pop )
#include <offload.h>
#include "aux.h"

// -- function headings -- 
void xfer_to_mic_pack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

void xfer_to_mic( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void xfer_to_mic_semipack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void dealloc_on_mic_pack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

void dealloc_on_mic( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void iter_cpu_mic_pack( int n_iter, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

void iter_cpu_mic( int n_iter, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void iter_cpu_mic_semipack( int n_iter, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void mv_cpu_mic_pack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

void mv_cpu_mic( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

void mv_cpu_mic_semipack( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, double ** micApack = nullptr, 
  double ** micXpack = nullptr, double ** micYpack = nullptr );

#define N_MICS ( 2 )


//! main function
int main( 
  int argc, 
  char ** argv
  ){

  if( argc < 5 ){
    //! function parameters
    std::cout << "-test_gemv type mat_size n_mat n_iter" << std::endl;
    return 0;
  }
  
  int type = atoi( argv[ 1 ] );
  long n = atoi( argv[ 2 ] );
  long n_mat = atoi( argv[ 3 ] );
  int n_iter = atoi( argv[ 4 ] );
  //int chunkSize = atoi( argv[ 7 ]);
  int chunkSize = 5;

  std::cout << "type: ";
  if( type == 0 )
    std::cout << "gemv, ";
  if( type == 1 )
    std::cout << "symv, ";
  if( type == 2 )
    std::cout << "spmv, ";
  std::cout << "n: " << n << ", n_mat: " << n_mat << ", n_iter: " << 
    n_iter << std::endl;

  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;
  double * A = new double[ Asize ];
  double * x = new double[ n ];
  double * y = new double[ n ];
  memset( y, 0.0, n * sizeof( double ) );

  //generate_data( n, type, A, x );
  //print_vector( Asize, A );
  //print_vector( n, x );
  //print_vector( n, y );

  double * Apack = new double[ n_mat * Asize ];
  double * xpack = new double[ n_mat * n ];
  double * ypack = new double[ n_mat * n ];

  double * micApack [ N_MICS ];
  double * micXpack [ N_MICS ];
  double * micYpack [ N_MICS ];

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  
  generate_data( n, n_mat, type, Apack, xpack );


//! comment/uncomment and run individual test cases:

  //! runs offload packed version
  iter_cpu_mic_pack( n_iter, n, n_mat, type, Apack, xpack, ypack );
  //print_vector( n * n_mat , ypack );

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs individual offload version
  iter_cpu_mic_semipack( n_iter, n, n_mat, type, Apack, xpack, ypack, micApack, 
    micXpack, micYpack );
  //print_vector( n * n_mat , ypack );

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  //! runs individual offload version
  iter_cpu_mic( n_iter, n, n_mat, type, Apack, xpack, ypack, micApack, 
    micXpack, micYpack );
  //print_vector( n * n_mat , ypack );
  
  delete [] A;
  delete [] x;
  delete [] y;
  delete [] Apack;
  delete [] xpack;
  delete [] ypack;

  return 0;
}

//! offload pack
void iter_cpu_mic_pack(
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "packed cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "packed cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "packed cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  xfer_to_mic_pack( n, n_mat, type, Apack, xpack, ypack );

  gettimeofday( &t1, nullptr );
  
  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_cpu_mic_pack( n, n_mat, type, Apack, xpack, ypack );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/ 
  for( int i = 0; i < n_iter; ++i ){
     gettimeofday( &t3, nullptr );
     mv_cpu_mic_pack( n, n_mat, type, Apack, xpack, ypack );
     gettimeofday( &t4, nullptr );
     elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
     elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
     std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  
  gettimeofday( &t2, nullptr );
  
  dealloc_on_mic_pack( n, n_mat, type, Apack, xpack, ypack );
  
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "packed cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "packed cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "packed cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );
}

//! semi-packed offload
void  iter_cpu_mic_semipack(
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "semi-packed cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "semi-packed cblas_dsymv started" << std::endl;
      break;
    case 2:
      std::cout << "semi-packed cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  xfer_to_mic_semipack( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
    micYpack );

  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_cpu_mic_semipack( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
      micYpack );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_cpu_mic_semipack( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
      micYpack );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  
  gettimeofday( &t2, nullptr );
  
  dealloc_on_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
    micYpack );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;
  
  switch( type ){
    case 0:
      std::cout << "semi-packed cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "semi-packed cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "semi-packed cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );
}


//! individual offload
void  iter_cpu_mic(
  int n_iter,
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

  switch( type ){
    case 0:
      std::cout << "cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  xfer_to_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
    micYpack );

  gettimeofday( &t1, nullptr );

  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;
///*
  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_cpu_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
      micYpack );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
//*/
  for( int i = 0; i < n_iter; ++i ){
    gettimeofday( &t3, nullptr );
    mv_cpu_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
      micYpack );
    gettimeofday( &t4, nullptr );
    elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
    elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  
  gettimeofday( &t2, nullptr );
  
  dealloc_on_mic( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, 
    micYpack );
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;
  
  switch( type ){
    case 0:
      std::cout << "cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( );
}

//! tranfers packed data to mic using offload, allocates buffers
void xfer_to_mic_pack(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){
   
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ]; 
  long offset [ N_MICS ];
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < N_MICS; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];
  
#pragma omp parallel num_threads( N_MICS )
{
#pragma omp for schedule( static, 1 ) 
  for( int i = 0; i < N_MICS; ++i ){
    
    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

#pragma offload_transfer target( mic : i ) \
in( subApack : length( Asize * size[ i ] ) alloc_if( 1 ) free_if( 0 ) ) \
nocopy( subxpack : length( n * size[ i ] ) alloc_if( 1 ) free_if( 0 ) ) \
nocopy( subypack : length( n * size[ i ] ) alloc_if( 1 ) free_if( 0 ) ) 
  }
}
}

//! transfers individual data to mic using offload, allocates buffers
void xfer_to_mic_semipack(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){

  std::cout << " xfer_to_mix_semipack started " << std::endl;
   
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;
 
  int n_threads = omp_get_max_threads();
  int threads_per_mic = n_threads / N_MICS;

  long trgptr_size [ N_MICS ];
  long size [ n_threads ]; 
  long offset [ n_threads ];
  long offset_mic [n_threads ];
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    trgptr_size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++trgptr_size[ i ];

// nastavi size pro jednotliva CPU vlakna
  long idx;

  std::cout << "threads_per_mic = " << threads_per_mic << std::endl;
  

  for( int i = 0; i < N_MICS; ++i ){
    for( int j = 0; j < threads_per_mic; ++j ){
      idx = i * threads_per_mic + j;
      size[ idx ] = trgptr_size[ i ] / threads_per_mic; 
    }
    rest = trgptr_size[ i ] % threads_per_mic;
    for( long j = 0; j < rest; ++j )
      ++size[ i * threads_per_mic + j ];


    offset_mic[ i * threads_per_mic ] = 0;
    for( int j = 1; j < threads_per_mic; ++j ) {
      idx = i * threads_per_mic + j;
      offset_mic[ idx ] = offset_mic[ idx - 1] + size[ idx - 1 ];
    }
  }

  offset[ 0 ] = 0;
  for( int i = 1; i < n_threads; ++i )
    offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

/// offset_mic[ 12:23 ] = offset[ 12:23 ] - size_trgptr[ 0 ];

#pragma omp parallel
{

  long sizeA;
  long sizeX;
  long sizeY;
  double * A_p = nullptr;
  double * X_p = nullptr;
  double * Y_p = nullptr;


#pragma omp for schedule( static, 1 ) 
  for( int i = 0; i < N_MICS; ++i ){

    sizeA = trgptr_size[ i ] * Asize;
    sizeX = trgptr_size[ i ] * n;
    sizeY = trgptr_size[ i ] * n;
    
// Allocate memory on MICs
#pragma offload_transfer target( mic : i ) \
nocopy( A_p : length( sizeA ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( X_p : length( sizeX ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( Y_p : length( sizeY ) alloc_if( 1 ) free_if( 0 ) targetptr )

    micApack[ i ] = A_p;
    micXpack[ i ] = X_p;
    micYpack[ i ] = Y_p;

  }

  double * myA;
  double * micApack_p;

#pragma omp for
  for( int i = 0; i < n_threads; ++i ){
    sizeA = size[ i ] * Asize;
    myA = Apack + offset[ i ] * Asize;
    micApack_p = micApack[ i / threads_per_mic ] + ( offset_mic[ i ] * Asize );

// Transfer individual matrices from CPU to specific MIC, fit into the pre-allocated buffer
#pragma offload_transfer target( mic : ( i / threads_per_mic ) ) \
in( myA : length( sizeA ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr )

  }

}

}

//! transfers individual data to mic using offload, allocates buffers
void xfer_to_mic(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){
   
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ];
  long offset [ n_mat ];
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  
  for( long i = 0; i < n_mat; ++i ){
    offset[ i ] = i / N_MICS;
  }

#pragma omp parallel
{

  long sizeA;
  long sizeX;
  long sizeY;
  double * A_p = nullptr;
  double * X_p = nullptr;
  double * Y_p = nullptr;


#pragma omp for schedule( static, 1 ) 
  for( int i = 0; i < N_MICS; ++i ){

    sizeA = size[ i ] * Asize;
    sizeX = size[ i ] * n;
    sizeY = size[ i ] * n;
    
// Allocate memory on MICs
#pragma offload_transfer target( mic : i ) \
nocopy( A_p : length( sizeA ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( X_p : length( sizeX ) alloc_if( 1 ) free_if( 0 ) targetptr ) \
nocopy( Y_p : length( sizeY ) alloc_if( 1 ) free_if( 0 ) targetptr )

    micApack[ i ] = A_p;
    micXpack[ i ] = X_p;
    micYpack[ i ] = Y_p;

  }

  double * myA;
  double * micApack_p;

#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myA = Apack + i * Asize;
    micApack_p = micApack[ i % N_MICS ] + ( offset[ i ] * Asize );

// Transfer individual matrices from CPU to specific MIC, fit into the pre-allocated buffer
#pragma offload_transfer target( mic : ( i % N_MICS ) ) \
in( myA : length( Asize ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr )

  }

}

}

//! deallocates buffers on mic
void dealloc_on_mic_pack(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack
  ){
     
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ]; 
  long offset [ N_MICS ];
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < N_MICS; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];
  
#pragma omp parallel num_threads( N_MICS )
{
#pragma omp for schedule( static, 1 ) 
  for( int i = 0; i < N_MICS; ++i ){
    
    double * subApack = Apack + Asize * offset[ i ];
    double * subxpack = xpack + n * offset[ i ];
    double * subypack = ypack + n * offset[ i ];

#pragma offload_transfer target( mic : i ) \
in( subApack : length( 0 ) alloc_if( 0 ) free_if( 1 ) ) \
in( subxpack : length( 0 ) alloc_if( 0 ) free_if( 1 ) ) \
in( subypack : length( 0 ) alloc_if( 0 ) free_if( 1 ) )
  }
}
}

//! deallocates individual buffers on mic
void dealloc_on_mic(
  long n,
  long n_mat,
  int type,
  double * Apack,
  double * xpack,
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){
     
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

#pragma omp parallel num_threads( N_MICS )
{

  double * A_p;
  double * X_p;
  double * Y_p;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < N_MICS; ++i ){

  A_p = micApack[ i ];
  X_p = micXpack[ i ];
  Y_p = micYpack[ i ];

#pragma offload_transfer target( mic : i ) \
in( A_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr ) \
in( X_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr ) \
in( Y_p : length( 0 ) alloc_if( 0 ) free_if( 1 ) targetptr )
  }
}

}

//! performs offloaded packed matrix-vector multiplication
void mv_cpu_mic_pack( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ]; 
  long offset [ N_MICS ];
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < N_MICS; ++i )
    offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];
  
#pragma omp parallel num_threads( N_MICS )
{
#pragma omp for schedule( static, 1 )
  for( int i = 0; i < N_MICS; ++i ){
    
     double * subApack = Apack + Asize * offset[ i ];
     double * subxpack = xpack + n * offset[ i ];
     double * subypack = ypack + n * offset[ i ];

#pragma offload target( mic : i ) \
in( subApack : length( 0 ) alloc_if( 0 ) free_if( 0 ) ) \
in( subxpack : length( size[ i ] * n ) alloc_if( 0 ) free_if( 0 ) ) \
inout( subypack : length( size[ i ] * n ) alloc_if( 0 ) free_if( 0 ) )
{

#pragma omp parallel
{
  double * myX;
  double * myY;
  double * myA;
  long one = 1;

#pragma omp for
    for( long j = 0; j < size[ i ]; ++j ){
      myX = subxpack + j * n;
      myY = subypack + j * n;
      myA = subApack + j * Asize;
    
      switch( type ){
        case 0:
          cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, myA, n, myX, one, 0.0, myY, one );
          break;
        case 1:
          cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, myA, n, myX, one, 0.0, myY, one );
          break;
        case 2:
          cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, myA, myX, one, 0.0, myY, one );
          break;
      }
    }
}
}
  }
}
}

//! performs offloaded matrix-vector multiplication
void mv_cpu_mic_semipack( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  int n_threads = omp_get_max_threads();
  int threads_per_mic = n_threads / N_MICS;

  long trgptr_size [ N_MICS ];
  long size [ n_threads ]; 
  long offset [ n_threads ];
  long offset_mic [n_threads ]; 
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    trgptr_size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++trgptr_size[ i ];
  
// nastavi size pro jednotliva CPU vlakna
  long idx;

  for( int i = 0; i < N_MICS; ++i ){
    for( int j = 0; j < threads_per_mic; ++j ){
      idx = i * threads_per_mic + j;
      size[ idx ] = trgptr_size[ i ] / threads_per_mic; 
    }
    rest = trgptr_size[ i ] % threads_per_mic;
    for( long j = 0; j < rest; ++j )
      ++size[ i * threads_per_mic + j ];


    offset_mic[ i * threads_per_mic ] = 0;
    for( int j = 1; j < threads_per_mic; ++j ) {
      idx = i * threads_per_mic + j;
      offset_mic[ idx ] = offset_mic[ idx - 1] + size[ idx - 1 ];
    }
  }

  offset[ 0 ] = 0;
  for( int i = 1; i < n_threads; ++i )
    offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

/// offset_mic[ 12:23 ] = offset[ 12:23 ] - size_trgptr[ 0 ];
  
#pragma omp parallel
{
  double * myX_cpu;
  double * myY_cpu;
  double * myA_cpu;
  double * micApack_p;
  double * micXpack_p;
  double * micYpack_p;
  long sizeX;
  long sizeY;
  long size_semipack;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < n_threads; ++i ){
    myX_cpu = xpack + offset[ i ] * n;
    myY_cpu = ypack + offset[ i ] * n;
    myA_cpu = Apack + offset[ i ] * Asize;

    size_semipack = size[ i ];

    sizeX = size[ i ] * n;
    sizeY = size[ i ] * n; 

    micApack_p = micApack[ i / threads_per_mic ] + ( offset_mic[ i ] * Asize );
    micXpack_p = micXpack[ i / threads_per_mic ] + ( offset_mic[ i ] * n );
    micYpack_p = micYpack[ i / threads_per_mic ] + ( offset_mic[ i ] * n );

#pragma offload target( mic : ( i / threads_per_mic ) ) \
in( myA_cpu : length( 0 ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myX_cpu : length( sizeX ) into( micXpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myY_cpu : length( sizeY ) into( micYpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( size_semipack ) \
in( n ) \
in( n_threads )
{

  int mic_nthreads = omp_get_max_threads( ); 

#pragma omp parallel num_threads( mic_nthreads  / n_threads )
{
  double * myX;
  double * myY;
  double * myA;
  long one = 1;

#pragma omp for
  for( long j = 0; j < size_semipack; ++j ){
    myX = micXpack_p + j * n;
    myY = micYpack_p + j * n;
    myA = micApack_p + j * Asize;
 
    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break;
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, myA, myX, one, 0.0, myY, one );
        break;
     }
    
  }
}
}

#pragma offload_transfer target( mic : ( i / threads_per_mic ) ) \
out( micYpack_p : length( sizeY ) into( myY_cpu ) alloc_if( 0 ) free_if( 0 ) targetptr )

  }

}

}


//! performs offloaded matrix-vector multiplication
void mv_cpu_mic( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack,
  double ** micApack,
  double ** micXpack,
  double ** micYpack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ];
  long offset [ n_mat ];
  
  long sizeall = n_mat / N_MICS;
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  
  for( long i = 0; i < n_mat; ++i ){
    offset[ i ] = i / N_MICS;
  }

#pragma omp parallel
{
  double * myX_cpu;
  double * myY_cpu;
  double * myA_cpu;
  double * micApack_p;
  double * micXpack_p;
  double * micYpack_p;
  long one = 1;


#pragma omp for
  for( long i = 0; i < n_mat; ++i ){
    myX_cpu = xpack + i * n;
    myY_cpu = ypack + i * n;
    myA_cpu = Apack + i * Asize;

    micApack_p = micApack[ i % N_MICS ] + ( offset[ i ] * Asize );
    micXpack_p = micXpack[ i % N_MICS ] + ( offset[ i ] * n );
    micYpack_p = micYpack[ i % N_MICS ] + ( offset[ i ] * n );

#pragma offload target( mic : i ) \
in( myA_cpu : length( 0 ) into( micApack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myX_cpu : length( n ) into( micXpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
in( myY_cpu : length( n ) into( micYpack_p ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
signal( micYpack_p )
{
    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, micApack_p, n, micXpack_p, one, 0.0, micYpack_p, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, micApack_p, n, micXpack_p, one, 0.0, micYpack_p, one );
        break;
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, micApack_p, micXpack_p, one, 0.0, micYpack_p, one );
        break;
     }
}
  }


#pragma omp for schedule( static, 1 )
  for( long i = 0; i < n_mat; ++i ){
    myY_cpu = ypack + i * n;
    micYpack_p = micYpack[ i % N_MICS ] + ( offset[ i ] * n );

#pragma offload_transfer target( mic : ( i % N_MICS ) ) \
out( micYpack_p : length( n ) into( myY_cpu ) alloc_if( 0 ) free_if( 0 ) targetptr ) \
wait( micYpack_p )
  }

}

}

