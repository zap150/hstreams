# Matrix-Vector Multiplication using offload & hStreams
This repository serves as a benchmark for comparison of Intel Xeon Phi offload runtimes. Our implementation covers three different versions using Intel Language Extansions (LEO) and the Hetero Streams Library (hStreams). As a benchmark we use the dense matrix-vector multiplication.

--------------------------------------------------------------------------------
### Brief explanation of directory tree of source code distribution of the Hetero Streams Library.

Directory                 | Description
:-------------------------|:----------------------------------------------------
./test_gemv               | Experimental code


--------------------------------------------------------------------------------
### Building and running the test code
Set up the Intel (R) C++ Composer XE environment. Then run make to build. Read run.sh file to run the application. 

